# inChurch DevOps Recruitment

This challenge seeks to test your knowledge as Devops. The scope of this challenge is purposefully open and you are welcome to implement it in varying degrees of complexity.

### Requirements
- AWS account (if you don't have one, create a free tier account)

## Application

Create a Python API project that calculates the speed of an object, taking 2 parameters: the distance covered (in meters) and the time interval (in seconds).

### GET /api/speed

The endpoint accepts the following parameters

| Parameter | Description | Type |
|---|---|---|
| distance (reqeuired) | Distance traveled | int |
| time (required) | Time to travel the distance | int |


### Examples

- Request
```
GET http://127.0.0.1:8080/api/speed?distance=100&time=10
```

- Success Response

```
Status: 200
```
``` json
{
    ​"speed": "10.0 m/s"
}
```

- Error Response (use an appropriate error message)

```
Status: 400
```
``` json
{
    "error_message": "The parameter 'time' is required"
}
```

### Important!
You must write tests for your application


## CI/CD

The next step is to create a CI/CD pipeline for your application

- Create a Docker container for the project
- Deploy your application to an AWS server (using Infrastructure as Code is a plus).
- Create a CI/CD pipeline using the technologies of your choice so that any commit on the master that passes all tests must be deployed


## What Should I deliver?
- Fork this repository (Click on the '+' button on the left menu and then on 'Fork this repository')
- Commit the application as well as Docker related files and any other files related to infrastructure or CI/CD
- Set your AWS environment
- Describe in the README your technical choices justifying each one
- Describe in the README what you would like to have done if you had more time
- Be prepared for presenting your CI/CD pipeline in action during the interview (you will be sharing your screen)


## What is going to be evaluated?
- Code organization
- Technical choices: Justification for choosing libraries, architecture, etc.
- Security: Are there any vulnerabilities that have not been reported?
- Unit tests


## Do you have any questions? ##

Contact: rafael.reis@inchurch.com.br